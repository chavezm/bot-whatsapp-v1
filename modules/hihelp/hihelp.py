from app.mac import mac, signals

'''
Signals this module listents to:
1. When a message is received (signals.command_received)
==========================================================
'''
@signals.command_received.connect
def handle(message):
    if message.command == "vv":
        hi(message)
    elif message.command == "menu": 
        menu(message)

'''
Actual module code
==========================================================
'''
def hi(message):
    who_name = message.who_name
    answer = "Fue desarrollado por Software Factory Desarrolladores: \n Joselyne Nives \n Pierre Chavez \n Armando Chavez"
    mac.send_message(answer, message.conversation)
    
def menu(message):
    answer = " 🔷🔷 Menu de  Ayuda ✅ 🔷🔷 \n Escribe : \n 1.- notas \n 2.-deudas  \n 3.- !verfoto 593999999999 \n 4.- !mifoto"
    mac.send_message(answer, message.conversation)
